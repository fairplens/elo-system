from flask import Flask, url_for, redirect
from authlib.integrations.flask_client import OAuth

# init flask app
app = Flask(__name__)
app.config.from_object("config.DevelopmentConfig")

# init OAuth app with Flask integration
oauth = OAuth(app)

# OAuth configuration
oauth.register(
        name="note",
        client_id=app.config["CLIENT_ID"],
        client_secret=app.config["CLIENT_SECRET"],
        access_token_url="https://note.crans.org/o/token/",
        authorize_url="https://note.crans.org/o/authorize/",
        api_base_url="https://note.crans.org/api/",
        client_kwargs={
            "scope":"1_1 2_1 3_1",
            },
        )

@app.route('/login')
def login():
    redirect_uri = url_for('authorize', _external=True)
    print(redirect_uri)
    return oauth.note.authorize_redirect(redirect_uri)

@app.route('/authorize')
def authorize():
    token = oauth.note.authorize_access_token()
    print(token)
    resp = oauth.note.get('me')
    resp.raise_for_status()
    profile = resp.json()
    print(profile)
    # do something with the token and profile
    return redirect('/')
