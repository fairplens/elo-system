# EloSystem

This is a simple elo system, using OAuth with the nk20.
The goal is to have an elo system for Fairplens.

## Installation

Rename `config.py.example` to `config.py` and edit the file to add the correct
variables.
To generate `SECRET_KEY` you can use:
```
python -c 'import secrets; print(secrets.token_hex())'
```

Create an environnement with the correct requirements:
```
python3 -m venv env
. env/bin/active
pip3 install -r requirements.txt
```

Deactivate the environnement:

```
deactivate
```

## Configuration

For OAuth config, please read: https://note.crans.org/doc/external_services/oauth2/

## Production

WARNING: do not use these settings for production.
See the Flask documentation for more information.

## Running

To run the programm:
```
./run.sh
```

